# -*- encoding: utf-8 -*-

"""
Configurtion and Controller of `dPramanik` Telegram BOT

The Telegram BOT is designed to let an enduser query and find basic
details about myself before directly contacting me for help. There
are several protocols and implementations (TODO) to be added in such
a way that a complete picture is presented via the chatbot.
"""

from src import * # noqa: F403 # pylint: disable=unused-import
from aiogram import executor

if __name__ == "__main__":
    executor.start_polling(dp, skip_updates = True)
