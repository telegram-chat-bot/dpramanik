# Changelog

<div align = "justify">

<p>All notable changes to this <code><b>repository</b></code> will be documented here. The format is based on <a href = "https://keepachangelog.com/en/1.0.0/">Keep a Changelog</a>, and this project adheres to <a href = "https://semver.org/spec/v2.0.0.html">Semantic Versioning</a>. For more details, check <a href = "https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/creating-a-default-community-health-file">documentation</a> about creating community health files.</p>

## Release Version: [v0.1.0]()
Release Date: 09-10-2022

<p>A <i>heuristic</i> chatbot designed using <code>aiogram</code> that uses the <a href = "https://core.telegram.org/bots/api">Telegram BOT API</a> in the backend to provide social media links to any enduser.</p>

</div>
