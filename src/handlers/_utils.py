# -*- encoding: utf-8 -*-

from src import ROOT
from os.path import join

def _get_commands_content(filename : str) -> str:
    # get the content of the file in `raw` format by invoking the
    # `open().read()` on the specified file name.
    # the function can be called by any `handlers` to fet the content
    # with ease.
    content = open(
        join(ROOT, "static", "commands", filename),
        encoding = "utf-8"
    ).read().strip()

    return content
