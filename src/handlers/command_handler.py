# -*- encoding: utf-8 -*-

"""
API Handler for Commands like `/start`, `/help` etc. are defined and
controlled from this file. This usses the special attributes which is
a part of the `message_handler` object.
"""

import yaml

from os.path import join
from aiogram import types
from urllib.request import urlopen
from aiogram.types import ReplyKeyboardMarkup

from src import dp, ROOT
from src.handlers._utils import _get_commands_content

@dp.message_handler(commands = ["start"])
async def start(message: types.Message) -> bool:
    markup = ReplyKeyboardMarkup(resize_keyboard = True)
    markup.row("test-user", "Debmalya Pramanik")

    await message.answer(
        _get_commands_content(filename = "start.partial.html").format(
            username = message.from_user.full_name
        )
    )
    return True


@dp.message_handler(commands = ["help"])
async def help(message: types.Message):
    # ? help documentation is controlled using the static file content

    with open(join(ROOT, "static", "commands", "commands.master"), "r") as f:
        # read the commands as used for bot configuration
        # https://stackoverflow.com/a/3277516/6623589
        commands = []
        for line in f:
            line = f"  #️⃣ /{line.strip()}"
            # also inject html object, to make it prettier
            # TODO check auto-type functionality after HTML injection
            c, d = line.split(" - ") # command, description
            commands.append(f"<b>{c}</b> - {d}")

        commands = "\n".join(commands)
        await message.answer(_get_commands_content(filename = "help.partial.html").format(
            version = "{{ VERSION: " + open(join(ROOT, "VERSION"), "r").read() + " }}",
            documentation = "https://gitlab.com/telegram-chat-bot/dpramanik/-/blob/master/CHANGELOG.md",
            commands = commands
        ))


@dp.message_handler(commands = ["link"])
async def link(message: types.Message) -> bool:
    """Prompt any Social Media Link to the User based on Argument"""

    with open(join(ROOT, "config", "links.yml"), "r") as f:
        link_ = yaml.load(f, Loader = yaml.FullLoader)["link"]

    arguments = message.get_args().lower()

    content = urlopen(link_)
    content = yaml.load(content, Loader = yaml.FullLoader)

    # ! since all links are not categorized, define a dictionary parsed from `content`
    # contains all links, which can be used for `--all` and `name` functionalities
    _all_links_ = {
        k : v for category in content["links"].keys()
        for k, v in content["links"][category]["websites"].items()
    }

    if (arguments in ["--help", "—help", "--group", "—group"]) or not arguments: # ? telegram (web) autoformats `--` to `—`
        # format the `categories` and `decription` by injecting codecs
        categories_with_description = "\n".join([
            f"""  #️⃣ <b>{category} :</b> <i>{content['links'][category]['description']}</i>
              {content['links'][category]['websites'].keys()}"""

            for category in content["links"].keys()
        ])

        message_ = _get_commands_content(filename = "connect_document.partial.html").format(
            categories_with_description = categories_with_description
        )
        await message.answer(message_)

    else:
        # * if `--all` is sent, then add all available keys
        # ? send all links as seperate message
        if arguments in ["--all", "—all"]:
            arguments = _all_links_.keys()
        elif ("--group" in arguments) or ("—group" in arguments):
            category_name_ = arguments.split()[-1]
            # TODO send error/warning if more than one category is requested
            
            arguments = content["links"][category_name_]["websites"].keys()
        else:
            arguments = arguments.split()

        for argument in arguments:
            try:
                message_ = _all_links_[argument]["link"]
                message_ = _get_commands_content(filename = "connect_success.partial.html").format(
                    social_site = argument,
                    link = message_
                )
            except KeyError:
                message_ = _get_commands_content(filename = "connect_failed.partial.html").format(
                    social_site = argument
                )

            await message.answer(message_)
