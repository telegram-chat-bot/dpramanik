# -*- encoding: utf-8 -*-

"""
Default Message when the Command/Content is not Understood
"""

from time import ctime
from aiogram import types
from aiogram.types import ReplyKeyboardMarkup

from src import dp
from src.handlers._utils import _get_commands_content

@dp.message_handler()
async def echo(message: types.Message):
    # ! default `fallback` handler
    # this response is sent out when the message is not configured
    markup = ReplyKeyboardMarkup(resize_keyboard = True)
    markup.row("test-user", "Debmalya Pramanik")

    await message.answer(
        _get_commands_content(filename = "default.partial.html").format(
            username = message.from_user.full_name,
            documentation = "https://gitlab.com/telegram-chat-bot/dpramanik/-/blob/master/CHANGELOG.md",
            messageID = message.message_id,
            content = message.text,
            datetime = ctime()
        )
    )
