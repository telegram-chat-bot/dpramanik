# -*- encoding: utf-8 -*-

"""
Handlers for Commands and Message(s) Responses

A set of handlers are defined, which may either work independently,
or may work in association with `agents` and/or `engines`, etc. to
send automated reply via the chatbot.
"""

from src.handlers.command_handler import * # noqa: F403 # pylint: disable=unused-import
from src.handlers.default_handler import * # noqa: F403 # pylint: disable=unused-import
