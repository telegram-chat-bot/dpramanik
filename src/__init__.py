# -*- encoding: utf-8 -*-

"""
Initialization File for the Telegram BOT

Use this file to define the global configurations and initialize
`bot` and `dp` (dispatcher) from the `aiogram` module to start the
API services.

The `src` is initialized as an 'python module' and is thus available
to be called from `dpramanik.py` which is the main controller file
for the module. In addition, all the submodules can access the
object as `from src import dp, bot` where required.
"""

import os
import time
import yaml

from aiogram import Bot, Dispatcher, types

def _get_token(path : str, verbose : bool = True) -> str:
    # ! get the token of telegram bot
    # the token is available when creating the bot, or get the same
    # on edit bot menu on `botfather`

    with open(path, "r") as f:
        meta = yaml.load(f, Loader = yaml.FullLoader)
    
    if verbose:
        print(f"{time.ctime()} Fetching `AUTH_TOKEN` from Settings File <v{meta['version']}> Dated: {meta['about']['date']}")

    return os.getenv(meta["token"]["name"])


# ! ROOT: the root path is a special variable for accessing static
# contents for handling the BOT, for fetching token, commands etc.
ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")

# ! defination of `bot` and `dispatcher` objects
bot = Bot(
    token = _get_token(path = os.path.join(ROOT, "config", "token.yml")),
    parse_mode = types.ParseMode.HTML
)
dp = Dispatcher(bot) # dispatcher object

# * init time option registrations
# ! important that the command handlers are initialized after bot/dp
from src.handlers import * # noqa: F403 # pylint: disable=unused-import
